# CC05

This repository is a living document for the Bachelor course Software-Based Products

## Documents

* [Overview](/overview.md)

* [Tools](/tools)
* [Programming](/programming)

* [Concepts](/concepts)
* [Lab XP](/xp)
* [Assignments](/assignments)

## Change Logs

### v0.3 Detailing the Experiments

### v0.2 Deliverable 2

* Learning objectives
* Teaching approach

### v0.1 Deliverable 1

* [Overview](/overview.md)
