---
name: Evaluating Bluetooth
type: Lab XP
version:
teacher:
techniques:
metrics: Robustness, distance, bandwidth
tools: Raspberry Pi
---

Why:

What:

How:

Use of the simulation/building infrastructure:


In this lab experiment:

* Establishing a Bluetooth P2P connection to explore the advantages and limits of this protocol
