---
name: Testing Methodologies
type: Lab XP
version:
teacher:
techniques:
metrics:
tools:
---

Why: Designers are the ones defining and providing requirement specifications to the software engineers
so that they develop pieces of software that match designer's concepts and expectation.

What: These requirements can be formulated as a list of test that software should pass to comply with
the requirements. In this lab XP, we will run existing test, define requirements and implement our own
software testing suit.

How: Unit, integration, functional, regression, system, performance, acceptance tests

Use of the simulation/building infrastructure:


In this lab experiment:

* 
