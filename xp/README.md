# Lab experiments

The purpose of the lab experiments is to experience technologies while learning feasibility testing methods.

Lab experiments are formative and group exercises.

Performance Benchmark: define metrics, collect and analyse data, draw conclusions.
* of a set of technical architectures
* of a set of algorithms complying to a list of requirements
* of a set of technologies or platforms

A/B test: develop and publish a prototype, set an experiment, collect and analyse data
* of two landing pages

Test the limits
* of a digital product security
* of a network bandwidth
* of data processing tasks
* of data ingestion tasks

Unit test an algorithm

Fault Tolerance

* [Using a Raspberry Pi as a Gateway/Hub to explore the possibilities](xp1.md)
* [Testing Methodology](xp2.md)
* [Hacking](xp3.md)
* [](xp4.md)
* [Establishing a Bluetooth P2P connection to explore the advantages and limits of this protocol](xp5.md)
* [](xp6.md)
* [](xp7.md)
* [](xp8.md)
* [](xp9.md)
