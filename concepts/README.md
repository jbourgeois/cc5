# Concept Units

The course is divided into 9 concept units, one per week, considering the last unit 'data journey'
as a revisiting units which does not provide new knowledge but aims at supporting students 
by looking at all covered concept units from the data perspective.

* [Digital Product Architectures](concept1.md)
* [Software Development](concept2.md)
* [Responsibility](concept3.md)
* [Digital Product Business Model](concept4.md)
* [Network Technologies](concept5.md)
* [Web Technology](concept6.md)
* [Algorithms - Specification](concept7.md)
* [Algorithms - Logic and data structures](concept8.md)
* [Data Journey](concept9.md)
