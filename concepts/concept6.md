---
name: Web Technology
type: Concept
version:
teacher:
---

# Description


# Video


# Quiz

Web technology is the set of methods by which computers communicate with each other through the use of markup languages and multimedia packages.
-	Network protocols: HTTP, MQTT, REST, WebSocket
-	Frontend: markup languages, components structure, responsiveness
-	Backend: API, libraries, servers
-	Analytics, services
