---
name: Software Development
type: Concept
version:
teacher:
---

# Description


# Video


# Quiz


-	Methodology: Agile, scrum, devOps (deployment), waterfall, rapid application, extreme programming, spiral
-	Software requirements and design patterns
-	Development life cycle
-	Software project management


Software Development Life Cycle (SDLC)


Waterfall, V-model, agile, spiral, rapid application development

Testing white/black box
