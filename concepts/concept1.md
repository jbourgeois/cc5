---
name: Digital Product Architecture
type: Concept
version:
teacher:
---

# Description


# Video


# Quiz


-	Components
o	Sensors and actuators
o	Gateways and Hub Devices
o	Cloud and Data platforms
-	Architectural considerations
o	Topologies
o	Spatial dimensions
-	Mesh networks, local gateway and direct connection
-	Human interfaces
-	Digital Twins
