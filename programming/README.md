# Programming

As part of the course, 2.5 ECs are dedicated to programming skills (70hrs in total)


Learning Objective: Students are able to read and write basic Python code (Literacy).

* Variables (8hrs): types, assignments, print formats, precision, operators (EWI Training 20 + project)
* Control flow (8hrs): for loops, while loops, conditions, and if-then-else statements (EWI Training 40 + project)
* Code Organization (8hrs): Indentation, execution flow, import, functions (EWI Training 60 + project)
* Basic Plotting (8hrs) (EWI Training 80 + project)


Learning Objective: Students are able to setup a Python environment on there own machine (Autonomy).

* Development environment (5hrs):Setting up development environment with Python, VS Code and Git

Learning Objective: Students are able to setup a prototyping platform such as Raspberry (Autonomy).

* Prototyping platform (8hrs): Setting up a Raspberry Pi for prototyping (installation, Linux-based environment, command line)
* Input/Output (6hrs): prototyping with basic sensor/actuator

Learning Objective: Students are able to develop a basic computer interface with Python and HTTP (Prototyping).

* API (8hrs): Building a REST API with Flask
