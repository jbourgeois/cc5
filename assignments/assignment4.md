---
name: Fitting in a business model in a given ecosystem
type: Assignment
version:
teacher:
concepts:
---

Why:

What:

How:



Without code

Exploring and leveraging the capabilities of a third-party service
Fitting in a business model in a given ecosystem

With code

Now that the business model is ready, let's build a simulation component wiring
a set of inputs and outputs with high-level function API, which achieve this.
