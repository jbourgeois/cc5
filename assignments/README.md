# Assignments

The purpose of the assignment is to apply some of the concepts covered in the course.

Assignments are formative and individual exercises.

## Deliverable

For each assignment, student will share two components:

* a short blog post
* a code snippet/gist

## Feedback


## List of Assignments

*[Breaking down an Existing Digital Product](assignment1.md)
*[](assignment2.md)
*[Mapping Privacy Concerns](assignment3.md)
*[Fitting in a business model in a given ecosystem](assignment4.md)
*[](assignment5.md)
*[Implementing a web page with live data visualisation](assignment6.md)
*[Specifying a Digital product with diagrams and pseudocode](assignment7.md)
*[](assignment8.md)
*[Smartphone Data](assignment9.md)
