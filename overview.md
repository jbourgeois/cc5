# Course Overview

## CourseBase Description

This course combines technology and business topics to teach students how to design and 
develop innovative digital products such as smart thermostat or pay-per-use washing machines.
Students will learn about digital business models and value proposition in relation to the
specification of a technical product architecture. Students will be trained to combine the
roles of product manager, product designer and product developer to design and realize a
digital prototype that is technically feasible and commercially viable. Students will develop
a hands-on understanding of core digital technologies and software development methods, and
will apply techniques and practices for iteratively creating, validating and improving
software-based products.


## Digital Consumer Products
This course focuses on consumer digital products. The home context is rich of such products, here are some examples:

* Smart thermostat such as Nest
* Smart doorbells and smart locks (from Ring, Nest, etc.)
* Smart speakers like Echo or Google Home
* Kitchen appliances such as Smart Fridges and oven
* Digital health Philips connected toothbrush and Withings bathroom scales
* White good such as pay-per-use connected washing machine such as Bundles and Homie
* Nest cams
* Pets trackers such as tractive
* Connected lighting

## Learning Objectives

2.5.2 ARCHITECTURE SPECIFICATION

Students are able to use semi-formal and formal methods to develop (alternative) architectures with respect to technical, business and sustainability aspects
* Concept unit 1 Digital product architecture
    * LO1.1 Students can explain the different types of system architecture available for consumer digital products.
    * LO1.2 Students are able to specify alternative, appropriate system architecture for consumer digital products that involve multiple software and hardware entities.

2.10.3 VALUE PROPOSITION

Students are able to set up a value proposition of a new design for an organization towards the consumer
* Concept unit 3 Responsible digital product development
    * LO3.1 Students are able to reflect on the design responsibility of consumer digital products from a technology perspective.
* Concept unit 4 Digital product value proposition and business model
    * LO3.2 Students are able to specify appropriate value propositions for consumer digital products.

2.10.2 BUSINESS MODELS

Students are able to distinguish between and apply different types of business models in designing a product-service system
* Concept unit 4 Digital product value proposition and business model
    * LO4.1 Students can explain the different types of business models available for consumer digital products.
    * LO4.2 Students are able to select an appropriate business model for consumer digital products that involve multiple software and hardware entities.

2.4.1 SOFTWARE DEVELOPMENT

Students are able to apply knowledge about algorithms, programming and software development methods to design, develop and analyze digital products and services
* Concept unit 2 Software development methods
    * LO2.1 Students are able to explain the purpose and context of application of agile, devOps, scrum, waterfall, rapid application, extreme programming and spiral
    * LO2.2 Students are able to carry out the development life cycle of a consumer digital product with a version control mechanism.
* Concept unit 8 Algorithm specification
    * LO8.1 Students are able to use pseudo code, flow chart and state machine diagrams to specify the algorithm driving the behaviour of a digital product.
    * LO8.2 Students are able to analyse the complexity of an algorithm.
* Concept unit 9 Algorithm logic
    * LO9.1 Students are able to express consumer digital product behaviours with algorithm logic.
    * LO9.2 Students are able to use programming knowledge to evaluate alternative algorithms.

2.4.3 NETWORKING AND CONNECTED SYSTEM

Students are able to apply knowledge about networking technologies, web technologies, Internet of Things and cloud computing to develop connected products and services
* Concept unit 5 Networking Technologies
    * LO5.1 Students are able to select appropriate network technologies among industry standards
    * LO5.2 Students are able to explain the communication models as well as the basics of information encoding and quantities
* Concept unit 6 Web Technology 
    * LO6.1 Students are able to select appropriate network protocols.
    * LO6.2 Students are able to select and specify Application Programming Interfaces (API).
* Concept unit 7 Data pipeline
    * LO7.1 Students are able to specify the data pipeline of a consumer digital product.
    * LO7.2 Students are able to explain the roles of edge and cloud computing

## Teaching Approach

The course uses a weekly cycle mixing theory and practice, thus a series of 9 formative units followed by an exam in the 10th week. Each week involve the introduction of theoretical concepts, a lab experiment and an assignment 

### Concepts (formative, individual)
Each week starts with a concept unit in the way they are handled in most MOOCs,
combining videos, transcripts and quizzes. The online quizzes are typical questions
that students should be able to answer during the final exam. This model gives students
flexibility to digest the material throughout the course while giving them the opportunity
to re-visit it before the exam. The lecturer in charge of a concept unit designs the video,
the quiz and the weekly assignment.

[See all concept units](/concepts)

### LAB EXPERIMENTS (FORMATIVE, GROUP)
A lab experiment leads students per group through a prototyping or testing techniques
to experience some of the theoretical concepts introduced during the week. It expends
the panel of protocols that students can use to inform and evaluate their design.
The expert in charge of a lab experiment prepare the written material and technical setup.
A video (e.g. http://talk.objc.io/episodes/S01E180-custom-button-styles?t=708) also helps
the students by demonstrating the expected outcome as well as walking them through the approach.
Lab experiments will lead students to mitigate the feasibility risk regarding algorithms,
performance, scalability, fault tolerance, unknown technologies and third-party services.
Each experiment will have two reading lenses: group (test results) and cohort level
(benchmark or statistics across all test results).

[See all lab experiments](/xp)

### Assignments (formative, individual)

Students put theoretical concept into practice in the individual assignment.
They can contain screencasts and guidance depending on the type of assignment.
Students capture this formative assignment as a blog post. They are given an
extra hour to comment and question the submission of other students.
Together with the lecturer of the week, the coordinator scans through the
blog post and the comments. They make a weekly podcast to provide feedback to
students, highlighting tips and tops, even suggesting looking at some selected
excellent assignment. This format is inspired by Peter Lloyd's DTM course.

[See all assignments](/assignments)


## Distribution of Hours

### Before the course
* 24hrs (3 days) for each lecturer (x9) to prepare each concept unit video and quiz
* 18hr for the coordinator to coordinate the video lecturing

* 16hrs (2 days) for each expert (x9) to develop a lab experiment
* 18hr for the coordinator to discuss/finalize the lab experiments

* 8hrs for each lecturer (x9) to develop the assignments
* 18hr for the coordinator to discuss/finalize the assignments and the quizzes

* 8hrs for the coordinator to design the exam
* 4hrs each for each assistant and experts (x3) to review/get acquainted to all lab experiment
### During the course (X9 Weeks)
* 4hrs for each expert and assistant (x12, average of 2 across 6 computer rooms)
* 1hrs extra for the expert responsible of the lab xp
* 1hrs for the coordinator and for the expert responsible of the lab xp to wrap up
* 2hrs for the lecturer to scan student questions in the forum and provide answers
* 6hrs for the coordinator and the lecturer to scan through the assignment and make a feedback podcast
* 1hrs for the coordinator to address admin issues/questions

### Exam week
* 4hrs for the coordinator and each assistant (x3)
* 1hr for the coordinator and each lecturer (x9) to prepare the grading

### After the course
* 8hrs for each expert for grading
* 2hrs for the coordinator and each lecturer (x9) to discuss the grades
* 2hrs for the coordinator to finalize the grading

