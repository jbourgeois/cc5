# Tools

## Software
 
### Blog platform

The blog platform such as Wordpress, hosted on IDE servers, will be the backbone of the course.

There we will post:
* the concepts, including videos (maybe a link to YouTube channel), quizzes and lecture notes,
* the lab xp with there intro and conclusion videos
* and assignments

These post will be open for comments, enabling students to ask question and offer remarks.

They will also post their assignment as a blog post. Seeing post of other students, we will
encourage peer voting (thumbs up) and commenting.

We will also post the weekly podcast.

The platform could integrate instant chat such as iFlyChat.

### Building Simulation in Python

To break down the complexity of connected products, a Python simulation enable students to experience 
connected products emulated on their laptop.

This simulation emulate a working ecosystem with inputs, outputs, processes, storage and communication.
It gives students the ability to break links between components, to replace components or 
to create there own components.

This simulation can also connect to the real-world, receiving inputs 

### Jupyter Notebook

We will use Jupyter Notebook to guide students through step by step Python exercises. This 

### Git and GitHub/GitLab


### Brightspace

Brightspace will be use for grading.

## Hardware

### Raspberry Pi 4

Together with a Grove Hat and custom Raspbian distribution, the advantage of the Raspberry Pi
over Arduino-like micro-controller is the diversity of activities students can explore and
the focus on Python as a single language while leaving open any other languages.

The challenge with Raspberry Pi is the initial setup which can require mouse, keyboard and screen
to get started.

To overcome this limitation, we need to prepare a dedicated Raspbian distribution
that embed a few extra script allowing the Raspberry Py to connect to Eduroam connection.
The students would login with there NetId on a web portal, which would trigger the preparation of
a custom distribution. Then, they would download their distribution ready to boot up with a given
hostname.

* Programming skills

As part of the Programming skills, students will learn how to get started on such platform
as well as explore the basics of electronics (sensor/actuators) and software (API) for prototyping.
[See Programming](../programming)

Example of Lab experiments

* [Evaluating Digital Architectures](../xp/xp1.md)
* [Evaluating Bluetooth](../xp/xp5.md)

Example of assignments

* 
*

### Input and Output devices with open APIs

All around the IDE building, we will gradually deploy simple devices, connected to the network with
documented APIs, letting student experiment with concrete input and outputs. This could includes
environment sensors, counters, light bulbs, speakers. They will provide the 'real-world' part of 
the building simulation, enabling students to experience concrete prototyping of software-based
products.


### iOS/Android App for easy input and output




## Support

* selling components to students
* maintaining a base of example codes

